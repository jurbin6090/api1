package com.example.demo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
@RestController
@CrossOrigin
public class DemoApplication {

    private static final Logger log = LogManager.getLogger(DemoApplication.class);

    @Value("${api2.url}")
    private String api2Url;

    @GetMapping("/")
    public String home() {
        log.info("/ called");
        return api1();
    }

    @GetMapping("/api1")
    public String api1() {
        log.info("/api1 called");
        return "Hello from api1";
    }

    @GetMapping("/remote")
    public String remote() {
        log.info("/remote called");
        String val = "Remote called";

        try {
            RestTemplate restTemplate = new RestTemplate();
            val = restTemplate.getForObject(api2Url, String.class);
        } catch (Exception ex) {
            log.error("Problem with remote call: " + ex.getLocalizedMessage(), ex);
        }

        return val;
    }

    @PostMapping("/remote2")
    public GenericResponse remote2(String uri) {
        log.info("/remote2 called for " + uri);
        String val = "### FAILED ###";
        try {
            RestTemplate restTemplate = new RestTemplate();
            GenericResponse val2 = restTemplate.postForObject(uri, null, GenericResponse.class);
            return val2;
        } catch (Exception ex) {
            val += "\n\nProblem with remote2 call[" + uri + "]: \n\n" + ex.getLocalizedMessage();
            log.error("Problem with remote2 call: " + ex.getLocalizedMessage(), ex);
        }
        return new GenericResponse();
    }

    private String version = "20210716.1440";

    @GetMapping("/version")
    public String version() {
        log.info("/version called");
        return version;
    }

    @GetMapping("/test")
    public String test() {
        return "test";
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
